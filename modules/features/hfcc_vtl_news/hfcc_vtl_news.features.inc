<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function hfcc_vtl_news_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function hfcc_vtl_news_node_info() {
  $items = array(
    'news' => array(
      'name' => t('News'),
      'module' => 'features',
      'description' => t('<em>News</em> articles are displayed on the home page and in an archive. Use <em>news</em> to post the latest updates about the VTL. The news section also provides an RSS feed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function hfcc_vtl_news_views_api() {
  return array(
    'api' => '2',
  );
}
